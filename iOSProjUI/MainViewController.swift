//
//  ViewController.swift
//  iOSProjUI
//
//  Created by Enoxs on 2020/3/3.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import UIKit

class MainViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    var lstFunc = [
        "Hello World" , "Auto Layout" , "Stack View" , "Simple Table" , "Custom Table" ,
        "Alert View" , "Table Action"
    ]
    
    // UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstFunc.count
    }
    // UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = "item"
        let cell = tableView.dequeueReusableCell(withIdentifier: id,for : indexPath)
        
        cell.textLabel?.text = lstFunc[indexPath.row]
        cell.imageView?.image = UIImage(named: "Apple")
        
        return cell
    }
    
    var lstItemSegueId = [
        "item01" , "item02" , "item03" , "item04" , "item05",
        "item06"
    ]
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let index = indexPath.row
//        let segueId = lstItemSegueId[index]
        
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "item01", sender: self)
        case 1:
            self.performSegue(withIdentifier: "item02", sender: self)
        case 2:
            self.performSegue(withIdentifier : "item03", sender: self)
        case 3:
            self.performSegue(withIdentifier: "item04", sender: self)
        case 4:
            self.performSegue(withIdentifier: "item05", sender: self)
        case 5:
            self.performSegue(withIdentifier: "item06", sender:self)
        case 6:
            self.performSegue(withIdentifier: "item07", sender:self)
        default:
            print("do nothing")
        }

//        self.performSegue(withIdentifier: segueId, sender:self)
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }

}
