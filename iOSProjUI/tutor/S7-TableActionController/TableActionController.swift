//
//  TableActionController.swift
//  iOSProjUI
//
//  Created by Enoxs on 2020/3/24.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import UIKit

class TableActionController: UITableViewController {
    
    var lstTitle = [
        "item01" , "item02" , "item03" , "item04" , "item05",
        "item06" , "item07" , "item08" , "item09" , "item10",
        "item11" , "item12" , "item13" , "item14" , "item15"
    ]
    var lstSubTitle = [
        "A","B","C","D","E",
        "F","G","H","I","J",
        "K","L","M","N","O"
    ]
    var lstContext = [
        "001" , "002" , "003" , "004" , "005",
        "006" , "007" , "008" , "009" , "010",
        "011" , "012" , "013" , "014" , "015"
    ]
    var lstImg = [
        "barrafina" , "bourkestreetbakery" , "cafedeadend" , "cafeloisl" , "cafelore",
        "barrafina" , "bourkestreetbakery" , "cafedeadend" , "cafeloisl" , "cafelore",
        "barrafina" , "bourkestreetbakery" , "cafedeadend" , "cafeloisl" , "cafelore"
    ]
    
    var lstIsSelect = Array(repeating: false, count: 15)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.cellLayoutMarginsFollowReadableWidth = true
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstTitle.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "item"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TableActionCell
        
        cell.txtTitle.text = lstTitle[indexPath.row]
        cell.txtSubTitle.text = lstSubTitle[indexPath.row]
        cell.txtContent.text = lstContext[indexPath.row]
        cell.imgIcon.image = UIImage(named: lstImg[indexPath.row])
        /*
         if lstIsSelect[indexPath.row] {
         cell.accessoryType = .checkmark
         } else {
         cell.accessoryType = .none
         }*/
        
        // Solution to Exercise #1
        cell.accessoryType = lstIsSelect[indexPath.row] ? .checkmark : .none
        cell.optionIcon.isHidden = true;
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Create an option menu as an action sheet
        let optionMenu = UIAlertController(title: nil, message: "Action", preferredStyle: .actionSheet)
        //
        if let popoverController = optionMenu.popoverPresentationController {
            if let cell = tableView.cellForRow(at: indexPath) {
                popoverController.sourceView = cell
                popoverController.sourceRect = cell.bounds
            }
        }
        
        // Add actions to the menu :
        let cancelAction = UIAlertAction(title: "Do nothing", style: .cancel, handler: nil)
        optionMenu.addAction(cancelAction)
        
        // Add Show Dialog action
        let showDialogHandle = { (action:UIAlertAction!) -> Void in
            let alertMessage = UIAlertController(title: "Dialog Title", message: "Dialog Message #\(indexPath.row+1)", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
        
        let showAction = UIAlertAction(title: "Show Dialog Message #\(indexPath.row+1)", style: .default, handler: showDialogHandle)
        optionMenu.addAction(showAction)
        
        let checkActionTitle = (lstIsSelect[indexPath.row]) ? "Undo Select" : "Select"
        
        // Select action
        let checkInAction = UIAlertAction(title: checkActionTitle, style: .destructive, handler: {
            (action:UIAlertAction!) -> Void in
            let cell = tableView.cellForRow(at: indexPath)
            self.lstIsSelect[indexPath.row] = !(self.lstIsSelect[indexPath.row])
            cell?.accessoryType = self.lstIsSelect[indexPath.row] ? .checkmark : .none
        })
        optionMenu.addAction(checkInAction)
        
        // Display the menu
        present(optionMenu, animated: true, completion: nil)
        
        // Deselect the row
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            // Delete the row from the data source
            self.lstTitle.remove(at: indexPath.row)
            self.lstSubTitle.remove(at: indexPath.row)
            self.lstContext.remove(at: indexPath.row)
            self.lstImg.remove(at: indexPath.row)
            self.lstIsSelect.remove(at: indexPath.row)
            
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Call completion handler with true to indicate
            completionHandler(true)
        }
        
        let shareAction = UIContextualAction(style: .normal, title: "Share") { (action, sourceView, completionHandler) in
            let defaultText = "Just checking in at " + self.lstTitle[indexPath.row]
            
            let activityController: UIActivityViewController
            
            if let imageToShare = UIImage(named: self.lstImg[indexPath.row]) {
                activityController = UIActivityViewController(activityItems: [defaultText, imageToShare], applicationActivities: nil)
            } else  {
                activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
            }
            
            if let popoverController = activityController.popoverPresentationController {
                if let cell = tableView.cellForRow(at: indexPath) {
                    popoverController.sourceView = cell
                    popoverController.sourceRect = cell.bounds
                }
            }
            
            self.present(activityController, animated: true, completion: nil)
            completionHandler(true)
        }
        
        // Set the icon and background color for the actions
        deleteAction.backgroundColor = UIColor(red: 231.0/255.0, green: 76.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        deleteAction.image = UIImage(named: "delete")
        
        shareAction.backgroundColor = UIColor(red: 254.0/255.0, green: 149.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        shareAction.image = UIImage(named: "share")
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
        
        return swipeConfiguration
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let checkInAction = UIContextualAction(style: .normal, title: "Check-in") { (action, sourceView, completionHandler) in
            
            let cell = tableView.cellForRow(at: indexPath) as! TableActionCell
            self.lstIsSelect[indexPath.row] = (self.lstIsSelect[indexPath.row]) ? false : true
            cell.optionIcon.isHidden = self.lstIsSelect[indexPath.row] ? false : true
            
            completionHandler(true)
        }
        
        let checkInIcon = lstIsSelect[indexPath.row] ? "undo" : "tick"
        checkInAction.backgroundColor = UIColor(red: 38.0/255.0, green: 162.0/255.0, blue: 78.0/255.0, alpha: 1.0)
        checkInAction.image = UIImage(named: checkInIcon)
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [checkInAction])
        
        
        return swipeConfiguration
    }
}
