//
//  CustomTableViewCell.swift
//  iOSProjUI
//
//  Created by Enoxs on 2020/3/15.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet var txtTitle: UILabel!
    @IBOutlet var txtSubTitle: UILabel!
    @IBOutlet var txtContent: UILabel!
    @IBOutlet var imgIcon: UIImageView!{
        didSet{ // 圖片圓角設定
            imgIcon.layer.cornerRadius = imgIcon.bounds.width / 2
            imgIcon.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
