//
//  ViewController.swift
//  iOSProjUI
//
//  Created by Enoxs on 2020/3/8.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import UIKit

class BasicViewController: UIViewController {

    @IBOutlet weak var txtConsole: UILabel!
    @IBOutlet weak var edtMsg : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        // Do any additional setup after loading the view.
    }
    
    @IBAction func modifyConsoleEvent(sender: UIButton){
        let text = edtMsg.text
        txtConsole.text = text
        edtMsg.text = "";
    }
    
    @IBAction func showMessageEvent(sender: UIButton){
        let selectedButton = sender
        if let labelText = selectedButton.titleLabel?.text {
            showDialog(msg: "iOSProjUI -> \(labelText)")
        }else{
            showDialog(msg : "iOSProjUI -> show Message")
        }
    }
    
    @IBAction func switchBtnEvent(sender: AnyObject){
        let btn = sender as! UISwitch
        if btn.isOn {
            setConsoleText(msg: "Switch -> ON")
        }else{
            setConsoleText(msg: "Switch -> OFF")
        }
    }
    
    
    func setConsoleText(msg : String){
        txtConsole.text = msg
    }

    func showDialog(msg : String ){
        let alertController = UIAlertController(title: "Dialog", message : msg , preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK",style: UIAlertAction.Style.default,handler: nil))
        present(alertController,animated: true,completion: nil)
    }

}
