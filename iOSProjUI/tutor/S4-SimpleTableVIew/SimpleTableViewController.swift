//
//  SimpleTableViewController.swift
//  iOSProjUI
//
//  Created by Enoxs on 2020/3/14.
//  Copyright © 2020 Enoxs. All rights reserved.
//

import UIKit

class SimpleTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    var lstTitle = ["001" , "002" , "003" , "004" , "005"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return lstTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = "item"
        let cell = tableView.dequeueReusableCell(withIdentifier: id,for : indexPath)
        cell.textLabel?.text = lstTitle[indexPath.row]
        cell.imageView?.image = UIImage(named: "Apple")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDialog(msg: "indexPath.row = \(indexPath.row)")
    }
    
    func showDialog(msg : String ){
        let alertController = UIAlertController(title: "Dialog", message : msg , preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK",style: UIAlertAction.Style.default,handler: nil))
        present(alertController,animated: true,completion: nil)
    }
    

}
